package main.java;

import org.apache.logging.log4j.Logger;

public class Main {
	private static final Logger LOG = Logger.getLogger(); 

	public static void main(String[] args) {
		
		while(true) {
			LOG.trace("Trace info");
			LOG.error("Trace error");
			LOG.warn("Trace warn");
			LOG.info("Trace info");
			LOG.debug("Trace debug");
		}
		
		Thread.sleep(10000);
	}

}
