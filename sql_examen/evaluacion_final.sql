--Seccion 1
--1. ¿Qué es el lenguaje DDL?
Data Definition Language, sentencias para crear, actualizar o borrar objetos de la BD

--2. Escriba en qué consiste el nivel READ UNCOMITTED en los niveles de aislamiento.
lecturas no confirmadas
permite leer los cambios realizados en una transacción que aún no estan comprometidos

--3. Explique en qué consiste la integridad referencial.
Permite asegurar que las referencias entre tablas sea integra. 
Por ejemplo no puedo borrar registros de una tabla padre si existen referencias desde una tabla hijo

--4. ¿Para qué nos sirve crear un índice?
Para agilizar el tiempo de respuesta de las consultas

--5. Explique para qué sirven los siguientes conceptos:
BEGIN. Inicio de una transacción
COMMIT Se comprometen todas las sentencias de la transacción
ROLLBACK Se da marcha atras a todas las sentencias ejecutadas en la transacción
SAVEPOINT Punto de salvamento de una transacción

--6. ¿Qué diferencia hay entre UNION Y JOIN?
Ambas permiten combinar columnas de diferentes tablas en un solo resultado.
la unión se utiliza para combinar filas.

--7. ¿Para qué sirve una cláusula CASE? Dé un ejemplo de una consulta.
Es una sentencia similar al IF else
CASE when estatus=1 then 'ACTIVO' else 'INACTIVO' END

--Sección 2
1. Cree la tabla alumno con las columnas id_alumno, nombre (50 caracters), num_cuenta (20 caracteres) y
licenciatura 15 caracteres.
La llave primaria es id_alumno, además num_cuenta y nombre no pueden ser nulos y el número
de cuenta debe ser único.
CREATE TABLE alumno( 
	id_alumno BIGINT PRIMARY KEY,
	nombre varchar(50) not null,
	num_cuenta varchar(20) not null,
	licenciatura varchar(15),
	CONSTRAINT UQ_numcuenta(num_cuenta)
);

2. Cree la tabla profesor con las columnas id_profesor, nombre(50 caracteres), num_empleado (10
caracteres), fecha de ingreso y fecha de retiro.
La llave primaria es id_profesor.
Nombre, número de empleado y fecha de ingreso no pueden ser nulos, además las fechas no
pueden ser posteriores al día de hoy.

CREATE TABLE profesor(
	id_profesor BIGINT PRIMARY KEY,
	nombre varchar(50) NOT NULL,
	num_empleado varchar(10) not null,
	fecha_ingreso date NOT NULL,
	fecha_retiro date,
	CONSTRAINT CK_FECHAPOS CHECK (fecha_ingreso <= current date() AND fecha_retiro <= current date())
);

3. Cree la tabla materia que tenga un id_materia, nombre_materia (30 caracteres), .

Se debe cuidar que id_alumno y id_profesor sean valores válidos dado que hacen referencia a--?? creo esta mal
otra tablas (alumno y profesor, respectivamente). --?? creo esta mal
Calificación debe estar entre cero y diez (incluyendo cero y diez). La calificación puede no estar--?? creo esta mal
asignada-- ?? creo esta mal

CREATE TABLE materia(
	id_materia BIGINT PRIMARY KEY,
	nombre_materia varchar(30)
)

4. Cree la tabla calificacion con los campos id_materia, id_alumno y id_profesor y calificación (todos
enteros excepto calificación que puede ser decimal).
la llave primaria es id_materia junto con id_alumno.
Los campos id_materia, id_alumno y id_profesor deben mantener la consistencia con la tabla
materia, alumno y profesor, respectivamente.
La calificación está entre cero y diez (incluyendo cero y diez)
CREATE TABLE calificacion(
	id_materia BIGINT NOT NULL,
	id_alumno BIGINT NOT NULL,
	id_profesor BIGINT NOT NULL,
	calificacion DECIMAL NOT NULL CHECK (calificacion >= 0 AND calificacion <= 10),
	CONSTRAINT FK_calificacion_materia (id_materia) REFERENCES materia(id_materia),
	CONSTRAINT FK_calificacion_alumno (id_alumno) REFERENCES alumno(id_alumno),
	CONSTRAINT FK_calificacion_profesor (id_profesor) REFERENCES profesor(id_profesor),
);

--5. Obtenga el nombre y el promedio de cada alumno.
SELECT a.nombre, AVG(c.calificacion)
FROM alumno a JOIN calificacion c on (a.id_alumno = c.id_alumno)
GROUP BY a.nombre

--6. Obtenga la materia que más ha sido reprobada
SELECT m.nombre_materia, COUNT(*)
FROM materia m JOIN calificacion c on (m.id_materia = c.id_materia)
WHERE c.calificacion < 7
GROUP BY m.nombre_materia
ORDER BY COUNT(*) desc LIMIT 1

--7. Obtenga el profesor que más personas ha reprobado (calificación mínima aprobatoria es 7).
SELECT p.nombre, COUNT(*)
FROM profesor p JOIN calificacion c on (p.id_profesor = c.id_profesor)
WHERE c.calificacion < 7
GROUP BY p.nombre
ORDER BY COUNT(*) desc LIMIT 1

--8. ¿Existe algún alumno que no tenga una calificación asociada?
SELECT a.nombre
FROM alumno a LEFT JOIN calificacion c on (a.id_alumno = c.id_alumno)
WHERE c is null
GROUP BY a.nombre

--9. Obtenga la lista de profesores que ya no están activos
SELECT *
FROM profesor
WHERE fecha_retiro is not null;

--10. ¿Existe alguna materia que jamás se haya dado?
SELECT m.nombre_materia
FROM materia m LEFT JOIN calificacion c on (m.id_materia = c.id_materia)
WHERE c is null
GROUP BY m.nombre_materia

--11. Obtenga el listado de profesores cuya fecha de ingreso fue anterior al año 2009
SELECT *
FROM profesor
WHERE extract( year from fecha_ingreso) < 2009
