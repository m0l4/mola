--1 ¿Cuántas órdenes hay por nación y región?

SELECT r.name, nk.name, count(*)
FROM orders o JOIN customer c on o.custkey = c.custkey
JOIN NATIONKEY nk on c.nationkey = nk.nationkey
JOIN REGION r on nk.regionkey = r.regionkey
GROUP BY r.name, nk.name

--2 ¿Todos los suppliers tienen un partsupplier?
SELECT s.name, count(ps.suppkey)
FROM SUPPLIER s LEFT JOIN PARTSUPP ps on s.suppkey = ps.suppkey
GROUP BY s.name

--3 ¿Cuántas órdenes hay por día de orden (orderdate)
SELECT o.orderdate, COUNT(*)
FROM ORDERS o
GROUP BY o.orderdate

--4 ¿Cuál es el promedio de impuestos (tax) pagados por año?
SELECT STRING(o.orderdate,"YEAR"), AVG(l.tax)
FROM orders o JOIN LINEITEM l on o.orderkey = l.orderkey
GROUP BY STRING(o.orderdate,"YEAR")

--5 ¿Cuál es el tamaño que más se repite en todos los comentarios?
SELECT LENGTH(o.comment), count(*)
FROM orders o
GROUP BY LENGTH(o.comment)
HAVING LENGTH(o.comment) = MAX(LENGTH(o.comment))

--6 ¿Cuál fue el orderpriority más utilizado?
SELECT o.order-priority, count(o.order-priority)
FROM orders o
group by o.order-priority
having max(count(o.order-priority)) = count(o.order-priority)

--7 ¿Existe una relación de la nación con el orderpriority?

